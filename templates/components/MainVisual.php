<section id="main-visual" class="main-visual">
	<div class="container">
		<h2 class="title-h1 main-visual__title">
			Vítejte na našich stránkách
		</h2>
		<div class="main-visual__content">
			<article class="main-visual__claim-text">
				<p>
					Naším cílem je nabídnout komplexní profesionální služby v oblasti elektroinstalačních a rekonstrukčních prací, budování STA televizního - satelitního příjmu a CCTV kamerového zabezpečovacího zařízení.
				</p>
				<a href="/#srvices" title="" class="btn btn--secondary btn--secondary--border">Naše služby</a>
			</article>
			<figure class="main-visual__img">
				<img src="./static/images/camera.png" alt="kamera - ilustrační obrázek" title="Elektroinstalační práce">
			</figure>
		</div>
	</div>
</section>