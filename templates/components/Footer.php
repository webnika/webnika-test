<footer class="footer section">
	<div class="container">
		<div class="footer--top">
		    <div class="contact-box">
		    	<div class="contact-box__title">
					<h2 class="title-h2--underline typo-color--primary">Rychlý kontakt</h2>
				</div>
				<address class="contact-box__contact-me">
					<p class="typo-title">Ondřej Áč</p>
						T: +420 600 300 300<br>
						E: ondrej.ac@email.cz
				</address>
				<address class="contact-box__address">
						Boční 1432/32, Ludgeřovice 747 14<br>
						IČ 87622211
				</address>
				<figure class="logo">
					<img src="./static/images/logo-text-dark.svg" alt="Áč - elektroinstalace" title="Áč - elektroinstalace">
				</figure>
		    </div>
		    <div class="contact-form-box">
		    	<div class="contact-form-box__title">
					<h2 class="title-h2--underline typo-color--primary">Máte dotaz?</h2>
				</div>
				<form method="post" class="form contact-form-box__form">   
					<input type="text" placeholder="Jméno" name="jmeno" data-validation="Vyplňte pole <strong>Jméno</strong>" required="required">
					<input type="text" placeholder="Příjmení" name="prijmeni" data-validation="Vyplňte pole <strong>Příjmení</strong>" required="required">
					<input type="text" placeholder="E-mail" name="email" data-validation="Vyplňte pole <strong>E-mail</strong>" required="required">
					<input type="text" placeholder="Telefon" name="phone" data-validation="Vyplňte pole <strong>Telefon</strong>">
					<textarea placeholder="Zpráva" name="zprava" data-validation="Vyplňte pole <strong>Zpráva</strong>" required="required"></textarea>
					<p class="gdpr">Odesláním formuláře souhlasíte se <a href="#" target="_blank" title="Zásady ochrany osobních údajů">zásadami ochrany osobních údajů</a>.</p>
					<div class="align align--right">
						<input type="submit" class="btn btn--secondary btn--right" value="Odeslat">
					</div>
	            </form>
				
		    </div>
		</div>
		<div class="footer--bottom">
			<nav class="navbar--footer">
                <ul class="nav__list">
                    <li class="nav__item">
                        <a class="nav__link" href="#">Úvod</a>
                    </li>
                    <li class="nav__item">
                        <a class="nav__link" href="#">Služby</a>
                    </li>
                    <li class="nav__item">
                        <a class="nav__link" href="#">Galerie</a>
                    </li>
                    <li class="nav__item">
                        <a class="nav__link" href="#">Kontakt</a>
                    </li>
                    <li class="nav__item">
                        <a class="nav__link" href="#">Zásady ochrany osobních údajů</a>
                    </li>
                </ul>
			</nav>
		</div>
	</div>
	<div class="footer__bg"></div>
</footer>