<section class="references section">
	<div class="container">
		<div class="references__title">
			<h2 class="title-h2--underline typo-color--primary">Reference</h2>
		</div>
		<div class="references--wrap">
			<div class="references__item--wrap">
				<div class="references__item--row">
					<a class="references__item" href="#" title="Více: Televizní - Satelitní příjem, reference Ostrava">
						<figure class="references__item--img">
							<img src="./static/images/references/reference-tv.jpg" alt="Televizní - Satelitní příjem, reference Ostrava" title="Televizní - Satelitní příjem, reference Ostrava">
							<div class="references__item--title">
								<h3 class="title-h3 typo-color--primary">Televizní - Satelitní příjem</h3>
								<h4 class="title-h4 typo-color--primary">Ostrava</h4>
							</div>
						</figure>
					</a>
					<a class="references__item" href="#" title="Více: Kamerové systémy, reference Hlučín">
						<figure class="references__item--img">
							<img src="./static/images/references/reference-camera.jpg" alt="Kamerové systémy, reference Hlučín" title="Kamerové systémy, reference Hlučín">
							<div class="references__item--title">
								<h3 class="title-h3 typo-color--primary">Kamerové systémy</h3>
								<h4 class="title-h4 typo-color--primary">Hlučín</h4>
							</div>
						</figure>
					</a>
				</div>
				<div class="references__item--row">
					<a class="references__item" href="#" title="Více: Elektroinstalační práce, reference Karviná">
						<figure class="references__item--img">
							<img src="./static/images/references/reference-electro.jpg" alt="Elektroinstalační práce, reference Karviná" title="Elektroinstalační práce, reference Karviná">
							<div class="references__item--title">
								<h3 class="title-h3 typo-color--primary">Elektroinstalační práce</h3>
								<h4 class="title-h4 typo-color--primary">Karviná</h4>
							</div>
						</figure>
					</a>
					<a class="references__item" href="#" title="Více: Stavební práce, reference Karviná">
						<figure class="references__item--img">
							<img src="./static/images/references/reference-construction.jpg" alt="Stavební práce, reference Karviná" title="Stavební práce, reference Karviná">
							<div class="references__item--title">
								<h3 class="title-h3 typo-color--primary">Stavební práce</h3>
								<h4 class="title-h4 typo-color--primary">Karviná</h4>
							</div>
						</figure>
					</a>
				</div>
			</div>
		</div>
		<div class="btn--wrap">
			<a class="btn btn--primary" href="#" title="Zobrazit všechny reference">Zobrazit všechny reference</a>
		</div>
	</div>
</section>

