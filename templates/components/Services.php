<section class="services section">
	<div class="container">
		<div class="services__title">
			<h2 class="title-h2--underline typo-color--primary">Naše služby</h2>
		</div>
		<div class="services__item--wrap">
			<div class="services__item">
				<a href="#" class="services__item--img">
					<figure>
						<img src="./static/images/services/service-tv.png" alt="Televizní - Satelitní příjem" title="Televizní - Satelitní příjem">
					</figure>
					<span class="badge badge--active">01</span>
				</a>
				<h3 class="title-h3 typo-color--primary">Televizní - Satelitní příjem</h3>
				<a class="typo-a" href="#" title="Více: Televizní - Satelitní příjem">více</a>
			</div>
			<div class="services__item">
				<a href="#" class="services__item--img">
					<figure>
						<img src="./static/images/services/service-camera.png" alt="Televizní - Kamerové systémy" title="Kamerové systémy">
					</figure>
					<span class="badge">02</span>
				</a>
				<h3 class="title-h3 typo-color--primary">Kamerové systémy</h3>
				<a class="typo-a" href="#" title="Více: Kamerové systémy">více</a>
			</div>
			<div class="services__item">
				<a href="#" class="services__item--img">
					<figure>
						<img src="./static/images/services/service-electro.png" alt="Elektroinstalační práce" title="Elektroinstalační práce">
					</figure>
					<span class="badge">03</span>
				</a>
				<h3 class="title-h3 typo-color--primary">Elektroinstalační práce</h3>
				<a class="typo-a" href="#" title="Více: Elektroinstalační práce">více</a>
			</div>
			<div class="services__item">
				<a href="#" class="services__item--img">
					<figure>
						<img src="./static/images/services/service-construction.png" alt="Stavební práce" title="Stavební práce">
					</figure>
					<span class="badge">04</span>
				</a>
				<h3 class="title-h3 typo-color--primary">Stavební práce</h3>
				<a class="typo-a" href="#" title="Více: Stavební práce">více</a>
			</div>
		</div>
	</div>
</section>

