<header class="header">
    <div class="container">
        <div class="header__wrap">
            <h1>
                <a href="#" title="">
                    <img class="logo" src="./static/images/logo.svg" alt="AČ elektroinstalace" title="AČ elektroinstalace">
                </a>
                <span class="text-hide">AČ - elektroinstalace</span>
            </h1>

            <nav class="navbar">
                <input class="navbar__btn" type="checkbox" id="menu-btn" />
                <label class="navbar__icon" for="menu-btn">
                    <span class="navicon"></span>
                </label>

                <ul class="nav__list">
                    <li class="nav__item nav__item--active">
                        <a class="nav__link" href="#">Úvod</a>
                    </li>
                    <li class="nav__item">
                        <a class="nav__link" href="#">Služby</a>
                    </li>
                    <li class="nav__item">
                        <a class="nav__link" href="#">Galerie</a>
                    </li>
                    <li class="nav__item">
                        <a class="nav__link" href="#">Kontakt</a>
                    </li>
                </ul>
            </nav>
        </div>
    </div>
</header>