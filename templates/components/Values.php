<section class="values section">
	<div class="container">
		<div class="values__wrap">
			<div class="values__item">
				<figure>
					<img src="./static/images/ico-hands.svg" alt="Spolehlivost" title="Spolehlivost">
				</figure>
				<h3 class="title-h3 typo-color--primary">Spolehlivost</h3>
			</div>
			<div class="values__item">
				<figure>
					<img src="./static/images/ico-ruler.svg" alt="Preciznost" title="Preciznost">
				</figure>
				<h3 class="title-h3 typo-color--primary">Preciznost</h3>
			</div>
			<div class="values__item">
				<figure>
					<img src="./static/images/ico-clock.svg" alt="Preciznost" title="Preciznost">
				</figure>
				<h3 class="title-h3 typo-color--primary">Dodržujeme termíny</h3>
			</div>
		</div>
	</div>
	<div class="values__bg"></div>
</section>