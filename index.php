<!DOCTYPE html>
<html lang="en">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <meta charSet="utf-8"/>
    <title>Veronika Petříková</title>
    <meta name="description" content="Testovací HTML / CSS stránka"/>
    <meta name="keywords" content="HTML,CSS,XML,JavaScript">
    <meta name="author" content="Veronika Petrikova - webnika.cz">

    <link rel="apple-touch-icon" sizes="152x152" href="./static/favicon/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="./static/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="./static/favicon/favicon-16x16.png">
    <link rel="manifest" href="./static/favicon/site.webmanifest">
    <link rel="mask-icon" href="./static/favicon//safari-pinned-tab.svg" color="#5bbad5">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="theme-color" content="#ffffff">

    <!-- SWIPER CSS -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.4.2/css/swiper.min.css">

    <!-- MAIN CSS -->
    <link rel="stylesheet" type="text/css" href="./style.min.css">
</head>
<body>
    <div class="page-wrap">
        <div class="page-homepage">
            <?php
            /**
                HEADER
            */
            include("./templates/components/Header.php"); 
            ?>

            <?php
            /**
               MAIN VISUAL
            */
            include("./templates/components/MainVisual.php"); 
            ?>

             <?php
            /**
               OUR VALUES
            */
            include("./templates/components/Values.php"); 
            ?>

             <?php
            /**
               SERVICES BOXES
            */
            include("./templates/components/Services.php"); 
            ?>

             <?php
            /**
               REFERENCES
            */
            include("./templates/components/References.php"); 
            ?>

            <?php 
            /**
                FOOTER
            */
            include("./templates/components/Footer.php"); 
            ?>
        </div>
    </div>
    <script
            src="https://code.jquery.com/jquery-3.3.1.min.js"
            integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
            crossorigin="anonymous"></script>
    <script src="./main.min.js"></script>
</body>
</html>