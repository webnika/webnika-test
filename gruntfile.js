module.exports = function(grunt) {

	grunt.initConfig({

		// Kompilace SCSS do CSS
		sass: {
			dist: {
				files: {
					'./stylesheets/grunt/style.css': './stylesheets/style.scss'
				}
			}
		},

		// Spojení javascriptu
		concat: {
			js: {
				src: './scripts/*.js',
				dest: './scripts/grunt/main.js'
			}
		},

		// Minifakce javascriptu a css
		uglify: {
			js: {
				files: {
					'./main.min.js': ['./scripts/grunt/main.js']
				}
			}
		},

	    // Minifikace 
	    cssmin: {
		  target: {
		    files: {
		      './style.min.css': ['./stylesheets/grunt/style.css']
		    }
		  }
		},

		// Stalker v akci
		watch: {
			js: {
				files: ['./scripts/*.js'],
				tasks: ['concat:js', 'uglify:js'],
				options: {
					spawn: false,
					livereload: true
				}
			},
			css: {
				files: ['./stylesheets/**/*.scss'],
				tasks: ['sass','cssmin'],
				options: {
					spawn: false,
					livereload: true
				}
			}
		},

	});

	// Naloaduj tasky
	grunt.loadNpmTasks('grunt-contrib-sass');
	grunt.loadNpmTasks('grunt-contrib-concat');
	grunt.loadNpmTasks('grunt-contrib-uglify');
	grunt.loadNpmTasks('grunt-contrib-cssmin');
	grunt.loadNpmTasks('grunt-contrib-watch');


	// Registruj spouštějící úlohy pro terminál - pro defaultní stačí volat "grunt"
	grunt.registerTask('default', [ 'sass','concat','uglify','cssmin','watch' ]);


};